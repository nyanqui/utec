$(document).ready(function() {
    var enabledScroll = true;
    var header_h = $("#navbar").position().top + $("#navbar").outerHeight();
    //HEADER

    var lineAnimation = function (el) {
        var $item = $(el);
        var item_l = $item.position().left;
        var item_w = $item.outerWidth();

        $(".main-nav__line").css({
            left: item_l,
            width: item_w
        });
    };

    lineAnimation(".main-nav__link--active");

    $(".main-nav__link").on("click", function (e) {
        var anchor = $(this).attr("href");

        enabledScroll = false;

        $("#nav-button").removeClass("main-nav__button--open");

        if ($(".header .container").outerWidth() < 970) {
            $(".collapse").slideUp();
        }

        $(".main-nav__link").removeClass("main-nav__link--active");
        $(this).addClass("main-nav__link--active");

        $('html,body').animate({
            scrollTop: $(anchor).offset().top - header_h
        }, 'slow', function () {
            enabledScroll = true;
        });

        e.preventDefault();
    });

    $(".header__logo, .footer-nav__link").on("click", function (e) {
        var anchor = $(this).attr("href");
        
        $('html,body').animate({
            scrollTop: $(anchor).offset().top - header_h
        }, 'slow');

        e.preventDefault();
    });

    $(".main-nav__link").hover(
        function () {
            lineAnimation(this);
        },
        function() {
            lineAnimation(".main-nav__link--active");
        }
    );

    //HEADER ANIMATION
    var header = $("#navbar").outerHeight();
    if ($("body").hasClass("admin-menu")) {
        header += 28;
    }
    var getOffset = function (id) {
        return $(id).offset().top + $(id).outerHeight();
    };

    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        var threshold = 400;

        if (scroll >= header) {
            $(".header").addClass("header--scrolling");
        } else {
            $(".header").removeClass("header--scrolling");
        }

        if (enabledScroll) {
            if (scroll >= 0 && scroll <= getOffset("#recuperar-ciudad") - threshold) {
                $(".main-nav__link").removeClass("main-nav__link--active");
                $(".main-nav__link[href='#recuperar-ciudad']").addClass("main-nav__link--active");
                lineAnimation(".main-nav__link--active");

            } else if (scroll >= $("#carrera-civil").offset().top - threshold && scroll <= getOffset("#carrera-civil") - threshold) {
                $(".main-nav__link").removeClass("main-nav__link--active");
                $(".main-nav__link[href='#carrera-civil']").addClass("main-nav__link--active");
                lineAnimation(".main-nav__link--active");

            } else if (scroll >= $("#pilares").offset().top - threshold && scroll <= getOffset("#pilares") - threshold) {
                $(".main-nav__link").removeClass("main-nav__link--active");
                $(".main-nav__link[href='#pilares']").addClass("main-nav__link--active");
                lineAnimation(".main-nav__link--active");

            } else if (scroll >= $("#articulos").offset().top - threshold && scroll <= getOffset("#articulos") - threshold) {
                $(".main-nav__link").removeClass("main-nav__link--active");
                $(".main-nav__link[href='#articulos']").addClass("main-nav__link--active");
                lineAnimation(".main-nav__link--active");

            } else if (scroll >= $("#postula").offset().top - threshold && scroll <= getOffset("#postula") - threshold) {
                $(".main-nav__link").removeClass("main-nav__link--active");
                $(".main-nav__link[href='#postula']").addClass("main-nav__link--active");
                lineAnimation(".main-nav__link--active");
            }
        }

    });


    $("#nav-button").on("click", function () {
        $(this).toggleClass("main-nav__button--open");
        $("#bs-example-navbar-collapse-1").slideToggle();
    });

     
    //LOCATION
    $("#location-around").on("click", function(e){
        $(".list-places").removeClass("open");
        $(".location-places").toggleClass("open-places");
        //$(".location-map").toggleClass("close-map");

        e.preventDefault();
    });

    $("#list-places").on("click", function(e){
        $(".location-places").removeClass("open-places");
        $(".list-places").toggleClass("open");
        //$(".location-map").toggleClass("close-map");
        e.preventDefault();
    });

    //CAROUSEL
    $('.carousel').owlCarousel({
        loop:true,
        margin:0,
        autoplay: true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            970:{
                items:4
            }
        }
    });

    $('.carousel-civil').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        items:1,
        dotsContainer: ".circle ul"
    });

    var owl = $('.carousel');
    owl.owlCarousel();
    $('.carousel-next').click(function() {
        owl.trigger('next.owl.carousel');
    });

    $('.carousel-prev').click(function() {
        owl.trigger('prev.owl.carousel');
    });
});
